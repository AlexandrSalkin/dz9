﻿using System;

namespace ДЗ9
{
    [AttributeUsage(AttributeTargets.All)]
    class MyClassAttribute : Attribute
    {
        string Name;
        string date;
        int task;
        public MyClassAttribute(string _Name, string _date, int _task)
        {
            Name = _Name;
            date = _date;
            task = _task;
        }
        
        public string NameS
        {
            get { return Name; }
        }

        public string dateD
        {
            get { return date; }
        }

        public int taskI
        {
            get { return task; }
        }
    }

    [MyClassAttribute("Alex", "2019-12-02", 11576)]
    class MyClass
    {

    }

    class Program
    {
        static void Main(string[] args)
        {
            Type tp = typeof(MyClass);
            Type tMyClassAtt = typeof(MyClassAttribute);
            MyClassAttribute mca = (MyClassAttribute)
                Attribute.GetCustomAttribute(tp, tMyClassAtt);

            Console.WriteLine(mca.NameS);
            Console.WriteLine(mca.dateD);
            Console.WriteLine(mca.taskI);
        }
    }
}
